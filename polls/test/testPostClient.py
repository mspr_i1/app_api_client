from django.test import TestCase
from ..models import Client
from django.urls import reverse
from django.test.client import Client as APIClient


class Test_create_client(TestCase):
    def test_post_create_client(self):
        # Création d'une instance de Client avec les données récupérées
        client = APIClient()
        response = client.post(f'/polls/create_client/', {
            "name": "Bajeux Alexis",
            "username": "abajeux",
            "firstname": "Alexis",
            "lastname": "Bajeux",
            "city": "Auxerre",
            "companyname": "R6Tracker",
            "password": "vzilvvfzl"
        })

        # Vérifie que la réponse a un statut 200 OK
        self.assertEqual(response.status_code, 200)

        print(response.json())
        # Retournez une réponse de succès
