# middleware.py
import time
from django.utils.deprecation import MiddlewareMixin
from .models import Supervision


class SupervisionMiddleware(MiddlewareMixin):
    def process_request(self, request):
        request.start_time = time.time()

    def process_response(self, request, response):
        exec_time = time.time() - request.start_time
        supervision = Supervision(
            request_return_code=response.status_code,
            query_exec_time=exec_time,
            nb_request=1
        )
        supervision.save()
        return response

    def process_exception(self, request, exception):
        exec_time = time.time() - request.start_time
        supervision = Supervision(
            request_return_code=500,  # Assuming 500 for server errors
            query_exec_time=exec_time,
            nb_request=1,
            exception_in_code=str(exception)
        )
        supervision.save()
