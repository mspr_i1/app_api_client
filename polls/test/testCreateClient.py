# Create your tests here.
from datetime import datetime

from polls.models import Client
import unittest


class TestUniter(unittest.TestCase):
    def test_client(self):
        lastname = "Musk"
        firstname = "Elon"
        name = "Elon Musk"
        username = "FuturHumanAI"
        postalcode = "89000"
        city = "Auxerre"
        companyname = "NeuraLink"
        password = "Elon814WYTesla"

        client = Client.objects.create(lastname=lastname, firstname=firstname, name=name,
                                       username=username, postalcode=postalcode,
                                       city=city, companyname=companyname, password=password)
        self.assertEqual(client.lastname, lastname)
        self.assertEqual(client.firstname, firstname)
        self.assertEqual(client.name, name)
        self.assertEqual(client.username, username)
        self.assertEqual(client.postalcode, postalcode)
        self.assertEqual(client.city, city)
        self.assertEqual(client.companyname, companyname)
        self.assertEqual(client.password, password)
