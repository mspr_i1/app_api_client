from django.test import TestCase
from ..models import Client
from django.test.client import Client as APIClient


class Test_getAllClientsDetails(TestCase):
    def test_get_client_details_by_all(self):
        Client.objects.create(
            lastname='Doe',
            firstname='John',
            name='Doe John',
            username='johndoe',
            postalcode='12345',
            city='New York',
            companyname='ABC Company',
            password='hupreqhuferpoq'
        )
        Client.objects.create(
            lastname='Don',
            firstname='James',
            name='James Don',
            username='JDon',
            postalcode='12345',
            city='Canada',
            companyname='Rockstar',
            password='gufreilg'
        )
        Client.objects.create(
            lastname='Morgan',
            firstname='Arthur',
            name='Arthur Morgan',
            username='AMorgan',
            postalcode='12345',
            city='Australia',
            companyname='StarGet',
            password='vfreilqf'
        )

        client = APIClient()
        response = client.get(f'/polls/client/')

        # Vérifie que la réponse a un statut 200 OKs
        self.assertEqual(response.status_code, 200)
        self.maxDiff = None
        self.assertEqual(str(response.json()), "[{'id': 1, 'lastname': 'Doe', 'firstname': 'John', 'name': 'Doe "
                                               "John', 'username': 'johndoe', 'postalcode': '12345', 'city': 'New "
                                               "York', 'companyname': 'ABC Company', 'password': 'hupreqhuferpoq'}, "
                                               "{'id': 2, 'lastname': 'Don', 'firstname': 'James', 'name': 'James "
                                               "Don', 'username': 'JDon', 'postalcode': '12345', 'city': 'Canada', "
                                               "'companyname': 'Rockstar', 'password': 'gufreilg'}, {'id': 3, "
                                               "'lastname': 'Morgan', 'firstname': 'Arthur', 'name': 'Arthur Morgan', "
                                               "'username': 'AMorgan', 'postalcode': '12345', 'city': 'Australia', "
                                               "'companyname': 'StarGet', 'password': 'vfreilqf'}]")


