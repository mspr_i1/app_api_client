import pika
from .models import Client

# Définissez les paramètres de connexion à RabbitMQ
RABBITMQ_HOST = '10.5.10.177'
RABBITMQ_PORT = 5672  # Notez que le port par défaut est 5672, et non 15672 qui est l'adresse de l'interface web de RabbitMQ
RABBITMQ_QUEUE = 'new_client_queue'  # Nom de la file d'attente où les messages seront envoyés


# Définissez la fonction pour envoyer un message à RabbitMQ
def send_message_to_rabbitmq(client):
    # Établissez la connexion à RabbitMQ
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=RABBITMQ_HOST, port=RABBITMQ_PORT))
    channel = connection.channel()

    # Déclarez la file d'attente si elle n'existe pas déjà
    channel.queue_declare(queue=RABBITMQ_QUEUE, durable=True)

    # Créez le message à envoyer
    message = {'id': client.id, 'date':client.date.isoformat()[:10],'name': client.name,'username':client.username,'firstname':client.firstname,'lastname':client.lastname,'postalcode':client.postalcode,'city':client.city,'companyname':client.companyname,'password':client.password}

    # Convertissez le message en JSON
    import json
    message_json = json.dumps(message)

    # Envoyez le message à RabbitMQ
    channel.basic_publish(exchange='', routing_key=RABBITMQ_QUEUE, body=message_json)

    # Fermez la connexion
    connection.close()


# Utilisez la fonction ci-dessus dans votre modèle de client pour envoyer un message à RabbitMQ lors de la création d'un nouveau client
from django.db.models.signals import post_save
from django.dispatch import receiver


@receiver(post_save, sender=Client)
def send_message_to_rabbitmq_on_client_creation(sender, instance, created, **kwargs):
    if created:
        send_message_to_rabbitmq(instance)
