import json
import pytest
from django.http import HttpResponse, HttpRequest, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from ..models import Client


@method_decorator(csrf_exempt, name='dispatch')
def delete_data_client_by_id(request, pk: int):
    if request.method == 'GET':
        # Récupérer les données du client
        client = Client.objects.get(id=pk)
        data = {
            'lastname': client.lastname,
            'firstname': client.firstname,
            'name': client.name,
            'username': client.username,
            'postalcode': client.postalcode,
            'city': client.city,
            'companyname': client.companyname,
            'password': client.password
            # Ajoutez d'autres champs au besoin
        }
        return JsonResponse(data)

    elif request.method == 'POST':
        # Supprimer le client avec l'ID spécifié
        client = Client.objects.get(id=pk)
        client.delete()
        return JsonResponse({'message': 'Les données du client ont été supprimées'})
