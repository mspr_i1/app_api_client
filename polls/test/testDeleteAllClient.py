from django.test import TestCase
from ..models import Client
from django.test.client import Client as APIClient


class Test_deleteAllClients(TestCase):
    def test_delete_all_client(self):
        Client.objects.create(
            lastname='Doe',
            firstname='John',
            name='Doe John',
            username='johndoe',
            postalcode='12345',
            city='New York',
            companyname='ABC Company',
            password='ugroeqguoerm'
        )
        Client.objects.create(
            lastname='Don',
            firstname='James',
            name='James Don',
            username='JDon',
            postalcode='12345',
            city='Canada',
            companyname='Rockstar',
            password='fireqgeqi'
        )
        Client.objects.create(
            lastname='Morgan',
            firstname='Arthur',
            name='Arthur Morgan',
            username='AMorgan',
            postalcode='12345',
            city='Australia',
            companyname='StarGet',
            password='uzmqhguerqls'
        )

        client = APIClient()
        response = client.post(f'/polls/delete_clients/')

        # Vérifie que la réponse a un statut 200 OKs
        self.assertEqual(response.status_code, 200)
