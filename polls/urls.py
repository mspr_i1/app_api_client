from django.urls import path
from .views import get_by_id
from .views.delete_all_client import delete_all_clients
from .views.get_all_user import getAllClientsDetails
from .views.get_by_id import getClientDetailsById
from .views.Post import create_client
from .views.delete_client_by_id import delete_data_client_by_id
from .views.update_data_by_id import update_data_client_by_id
urlpatterns = [
    path('client/<int:pk>/', getClientDetailsById),
    path('client/', getAllClientsDetails),
    path('create_client/', create_client.as_view()),
    path('delete_clients/', delete_all_clients),
    path('delete_client_by_id/<int:pk>/', delete_data_client_by_id),
    path('update_data_by_id/<int:pk>/', update_data_client_by_id)
]