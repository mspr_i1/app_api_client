from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from ..models import Client
#from polls import rabbitMQ_sender
import json


@method_decorator(csrf_exempt, name='dispatch')
class create_client(View):
    def post(self, request, *args, **kwargs):
        try:
            # Récupération des données de la requête POST
            name = request.POST.get('name', '')
            username = request.POST.get('username', '')
            firstname = request.POST.get('firstname', '')
            lastname = request.POST.get('lastname', '')
            postalcode = request.POST.get('postalcode', '')
            city = request.POST.get('city', '')
            companyname = request.POST.get('companyname', '')
            password = request.POST.get('password', '')

            # Création d'une instance de Client avec les données récupérées
            client = Client.objects.create(

                name=name,
                username=username,
                firstname=firstname,
                lastname=lastname,
                postalcode=postalcode,
                city=city,
                companyname=companyname,
                password=password
            )
            # Sauvegarde de l'instance dans la base de données
            client.save()
            #rabbitMQ_sender.send_message_to_rabbitmq(client)
            # Retournez une réponse de succès
            return JsonResponse({'message': 'Client créé avec succès !'})
        except json.JSONDecodeError:
            return JsonResponse({'error': 'Données JSON invalides'}, status=400)
