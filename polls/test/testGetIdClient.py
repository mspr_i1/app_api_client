from django.test import TestCase
from ..models import Client
from django.urls import reverse
from django.test.client import Client as APIClient


class TestGetClientByID(TestCase):
    def test_get_client_details_by_id(self):
        client = APIClient()
        client_instance = Client.objects.create(
            lastname='Doe',
            firstname='John',
            name='Doe John',
            username='johndoe',
            postalcode='12345',
            city='New York',
            companyname='ABC Company',
            password='uvigzulhf'
        )

        # Crée une requête GET pour récupérer les détails du client
        response = client.get(f'/polls/client/{client_instance.id}/')

        # Vérifie que la réponse a un statut 200 OK
        self.assertEqual(response.status_code, 200)

        # Affiche les données récupérées dans la console
        print(response.json())
