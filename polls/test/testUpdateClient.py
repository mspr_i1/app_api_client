from django.test import TestCase
from django.urls import reverse
from ..models import Client
from django.test.client import Client as APIClient


class UpdateDataClientByIdTestCase(TestCase):
    def test_update_data_client_by_id(self):
        client = APIClient()
        client_instance = Client.objects.create(
            lastname='Doe',
            firstname='John',
            name='Doe John',
            username='johndoe',
            postalcode='12345',
            city='New York',
            companyname='ABC Company',
            password='izlfrzulib'
        )

        # Crée une requête GET pour récupérer les détails du client
        response = client.get(f'/polls/client/{client_instance.id}/')
        # Vérifie que la réponse a un statut 200 OK
        self.assertEqual(response.status_code, 200)

        update = client.post(f'/polls/update_data_by_id/{client_instance.id}/', {
            "name": "Bajeux Alexis",
            "username": "abajeux",
            "firstname": "Alexis",
            "lastname": "Bajeux",
            "city": "Auxerre",
            "companyname": "R6Tracker",
            "password": "vfyrzif@giz`hghgkrudhu"
        })

        self.assertEqual(update.status_code, 200)

