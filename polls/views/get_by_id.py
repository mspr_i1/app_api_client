from django.http import HttpResponse, HttpRequest, JsonResponse
from rest_framework.decorators import api_view

from ..models import Client


@api_view(["GET"])
def getClientDetailsById(request,pk: int):
    # Recherche du client dans la base de données
    try:
        client = Client.objects.get(id=pk)
        data = {
            'lastname': client.lastname,
            'firstname': client.firstname,
            'name': client.name,
            'username': client.username,
            'postalcode': client.postalcode,
            'city': client.city,
            'companyname': client.companyname,
            'password': client.password
            # Ajoutez d'autres champs au besoin
        }
        return JsonResponse(data)
    except Client.DoesNotExist:
        return HttpResponse('Client non trouvé', status=404)
