FROM python:3.10

WORKDIR /usr/app/src

COPY . /usr/app/src

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 9620

CMD python manage.py runserver 0.0.0.0:9620
