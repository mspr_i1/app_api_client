import json

from django.http import HttpResponse, HttpRequest, JsonResponse
from rest_framework.decorators import api_view

from ..models import Client


@api_view(["GET"])
def getAllClientsDetails(request):
    clients = Client.objects.all()

    # Construire la liste des utilisateurs
    users_list = []
    for client in clients:
        user_data = {
            'id': client.id,
            'lastname': client.lastname,
            'firstname': client.firstname,
            'name': client.name,
            'username': client.username,
            'postalcode': client.postalcode,
            'city': client.city,
            'companyname': client.companyname,
            'password': client.password
            # Ajoutez d'autres champs au besoin
        }
        users_list.append(user_data)

    # Retourner la liste au format JSON

    # Retourner la réponse avec le JSON formaté
    return JsonResponse(users_list, safe=False)
