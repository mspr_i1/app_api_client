import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from ..models import Client


@csrf_exempt
def update_data_client_by_id(request, pk: int):
    try:
        if request.method == 'GET':
            # Récupérer les données du client
            client = Client.objects.get(id=pk)
            data = {
                'lastname': client.lastname,
                'firstname': client.firstname,
                'name': client.name,
                'username': client.username,
                'postalcode': client.postalcode,
                'city': client.city,
                'companyname': client.companyname,
                # Ajoutez d'autres champs au besoin
            }
            return JsonResponse(data)

        elif request.method == 'POST':
            # Récupération des données de la requête POST
            client = Client.objects.get(id=pk)
            name = request.POST.get('name', client.name)
            username = request.POST.get('username', client.username)
            firstname = request.POST.get('firstname', client.firstname)
            lastname = request.POST.get('lastname', client.lastname)
            postalcode = request.POST.get('postalcode', client.postalcode)
            city = request.POST.get('city', client.city)
            companyname = request.POST.get('companyname', client.companyname)
            password = request.POST.get('password', client.password)

            # Mettre à jour les données du client existant
            client.name = name
            client.username = username
            client.firstname = firstname
            client.lastname = lastname
            client.postalcode = postalcode
            client.city = city
            client.companyname = companyname
            client.password = password
            client.save()

            # Retournez une réponse de succès
            return JsonResponse({'message': 'Client mis à jour avec succès !'})
        else:
            return JsonResponse({'error': 'Méthode HTTP non autorisée'}, status=405)

    except Client.DoesNotExist:
        return JsonResponse({'error': 'Client non trouvé'}, status=404)

    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)
