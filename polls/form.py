from django import forms
from polls.models import Client
from django.forms import ModelForm


class ClientForm(ModelForm):
    class Meta:
        model = Client
        fields = ['ID', 'lastname', 'firstname', 'registrationdate', 'phonenumber', 'deliveryadress', 'company']
