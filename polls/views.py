from django.shortcuts import render

# Create your views here.
from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.views.generic import ListView

from polls.models import Client
from polls.form import ClientForm


class ClientListView(ListView):
    model = Client


class MyClientDetailView(ListView):
    model = Client

    def post(self, request):
        add_Client(request)
        return render(request, 'polls/Client_list.html', {'Client': Client})


def add_Client(request):
    if request.method == 'POST':
        form = ClientForm(request.POST)
        print(form)
        if form.is_valid():
            form.save()
