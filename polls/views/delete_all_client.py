import json

from django.http import HttpResponse, HttpRequest, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view

from ..models import Client


@method_decorator(csrf_exempt, name='dispatch')
def delete_all_clients(request):
    try:
        if request.method == 'POST':
            # Supprimer tous les clients
            Client.objects.all().delete()
            # Rediriger vers une page de confirmation ou une autre vue
            return JsonResponse({'message': 'Tout les clients sont supprimer'})
    except json.JSONDecodeError:
        return JsonResponse({'error': 'Impossible de supprimer la liste des client, Token Incorrect'}, status=400)
